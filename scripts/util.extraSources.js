        //This will return an array of objects
        //that is all of the *additional* sources of energy
        //storage in a room where the creep is located (so, Storages and Containers)
        //Harvesters will use these *before* the actual Source object in the room, then
        //once Spawn + Extensions are full, harvest the actual Source object and
        //top off the extra sources in this array
        
        var utilExtraSources = {

            /**
             * @param {Creep} creep
             * @param canUse
             */

            run: function(creep, canUse){

                //build array of extra storage spaces such as containers or storage 
                var sources = creep.room.find(FIND_SOURCES);

                var extraStorageStructures = creep.room.find(FIND_STRUCTURES, { 
                    filter: (structure) => { 
                        if (canUse){
                            return (((structure.structureType == STRUCTURE_CONTAINER) || (structure.structureType == STRUCTURE_STORAGE)) && (structure.store.getUsedCapacity(RESOURCE_ENERGY) > 0));
                        }
                        else{
                            return (((structure.structureType == STRUCTURE_CONTAINER) || (structure.structureType == STRUCTURE_STORAGE)) && (structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0));
                        }
                    }
                 });


                // push each item onto the end of sources so 
                // we end up with one array of all potential sources
                // for harvesters to pull from. later we will
                // prefer these additional storages over the root
                // energy sources for stability sake
                for (var i in extraStorageStructures){
                    sources.push(extraStorageStructures[i]);
                }

                //rebuild array with storages > containers > sources
                var sourcesFinal = [];
                for(var i in sources){
                    if(sources[i].structureType == STRUCTURE_STORAGE && !sourcesFinal.includes(sources[i])){
                        sourcesFinal.push(sources[i]);
                    }
                    if(sources[i].structureType == STRUCTURE_CONTAINER && !sourcesFinal.includes(sources[i])){
                        sourcesFinal.push(sources[i]);
                    }
                    if(sources[i].structureType == STRUCTURE_STORAGE && !sourcesFinal.includes(sources[i])){
                        sourcesFinal.push(sources[i]);
                    }
                }
                
                return sourcesFinal;
            }

        };

        module.exports = utilExtraSources;