var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleRepairDrone = require('role.repairdrone');
var workerSpawner = require('main.workerSpawner');
var rolePorter = require('role.porter');
var utilHeatMap = require('util.heatMap');
var roleWallMaintainer = require('role.wallmaintainer');

module.exports.loop = function () {

    console.log("*************");
    utilHeatMap.run(false);

    for(var name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }

    // quick and dirty defense
    for(room in Game.rooms){
        var hostiles = Game.rooms[room].find(FIND_HOSTILE_CREEPS);
        if(hostiles.length > 0) {
            var username = hostiles[0].owner.username;
            Game.notify(`User ${username} spotted in room ${room}`);
            var towers = Game.rooms[room].find(
                FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
            if((username !== 'How2rok') && (username !== 'NovemberBravo')){
                towers.forEach(tower => tower.attack(hostiles[0]));
        
            }
        }
        else{
            console.log("No attackers in Room " + room);
        }
    }

    var creepsList = Object.values(Game.creeps);
    var nextCreepDeath = creepsList[0].ticksToLive;
    var nextCreepName = creepsList[0].name;
    for (var creep in Object.values(Game.creeps)){
        if(creep.ticksToLive < nextCreepDeath){
            nextCreepDeath = creep.ticksToLive;
            nextCreepName = creep.name;
        }
    }

    console.log("Game time: " + Game.time);
    console.log("Next creep death is " + nextCreepName + " and it dies in " + nextCreepDeath + " ticks!");



    var harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'harvester');
    console.log('Harvesters: ' + harvesters.length);

    var upgraders = _.filter(Game.creeps, (creep) => creep.memory.role == 'upgrader');
    console.log('Upgraders: ' + upgraders.length);

    var builders = _.filter(Game.creeps, (creep) => creep.memory.role == 'builder');
    console.log('Builders: ' + builders.length);

    var repairdrones = _.filter(Game.creeps, (creep) => creep.memory.role == 'repairdrone');
    console.log('Repair Drones: ' + repairdrones.length);

    var porters = _.filter(Game.creeps, (creep) => creep.memory.role == 'porter');
    console.log('Porters: ' + porters.length);

    var wallmaintainers = _.filter(Game.creeps, (creep) => creep.memory.role == 'wallmaintainer');
    console.log('Wall maintainers: ' + wallmaintainers.length);


    //wrap spawner checks in here to make sure
    //it waits until we have full energy in the room
    for(var room in Game.rooms){
        //if((Game.rooms[room].energyCapacityAvailable - Game.rooms[room].energyAvailable) == 0){
        if((Game.rooms[room].energyAvailable / Game.rooms[room].energyCapacityAvailable) > 0.5){


            if(harvesters.length < 4) {
                for(var room in Game.rooms) {
                    workerSpawner.run(Game.rooms[room].energyAvailable, 'harvester'); 
                }      
            }
            
            else if(porters.length < 2) {
                for(var room in Game.rooms) {
                    workerSpawner.run(Game.rooms[room].energyAvailable, 'porter');
                }        
            }

            else if(repairdrones.length < 2) {
                for(var room in Game.rooms) {
                    workerSpawner.run(Game.rooms[room].energyAvailable, 'repairdrone');
                }        
            }

            else if(wallmaintainers.length < 1){
                for(var room in Game.rooms) {
                    workerSpawner.run(Game.rooms[room].energyAvailable, 'wallmaintainer');
                }
            }

            else if(upgraders.length < 1) {
                for(var room in Game.rooms) {
                    workerSpawner.run(Game.rooms[room].energyAvailable, 'upgrader');
                }        
            }          
        
            else if(builders.length < 0) {
                for(var room in Game.rooms) {
                    workerSpawner.run(Game.rooms[room].energyAvailable, 'builder');
                }        
            }
        
        }
    }

    


    if(Game.spawns['Spawn1'].spawning) { 
        var spawningCreep = Game.creeps[Game.spawns['Spawn1'].spawning.name];
        Game.spawns['Spawn1'].room.visual.text(
            'building => ' + spawningCreep.memory.role,
            Game.spawns['Spawn1'].pos.x + 1, 
            Game.spawns['Spawn1'].pos.y, 
            {align: 'left', opacity: 0.8});
    }

    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        if(creep.memory.role == 'harvester') {
            roleHarvester.run(creep);
        }
        if(creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
        }
        if(creep.memory.role == 'builder') {
            roleBuilder.run(creep);
        }
        if(creep.memory.role == 'repairdrone') {
            roleRepairDrone.run(creep);
        }
        if(creep.memory.role == 'porter') {
            rolePorter.run(creep);
        }
        if(creep.memory.role == 'wallmaintainer'){
            roleWallMaintainer.run(creep);
        }
    }


}

