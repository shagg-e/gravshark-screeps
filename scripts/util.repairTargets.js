
var utilRepairTargets = {

    /** 
     * @param {Creep} creep
     * @param needsWalls
     * **/
    run: function(creep, needsWalls) {
        var utilRepairTargets = creep.room.find(FIND_STRUCTURES, { 
            filter: (structure) => {
                if(!needsWalls){
                    return (structure.hits < structure.hitsMax) && (structure.hits > 0) && (structure.structureType !== STRUCTURE_WALL) && (structure.structureType !== STRUCTURE_RAMPART);
                }
                else{
                    return (structure.hits < structure.hitsMax) && (structure.hits > 0);
                }
            }
         });
	    return utilRepairTargets;

	}
};




module.exports = utilRepairTargets;