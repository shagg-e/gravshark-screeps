var workerSpawner = {

    /**
     * @param currentRoomEnergy
     * @param workerRole
     */
    run: function spawnWorker(currentRoomEnergy, workerRole){
        var costMove = 50; //init the three components
        var costCarry = 50; //initial costs
        var costWork = 100; 
        var numMove = 2; //init the three components
        var numCarry = 1; //min values for a basic worker
        var numWork = 1;
        var totalAccruedCost = 0; //this will increment up as we build the creep
        var partsArray = []; //base of the template array that we'll apply to the spawner

        // the juice => Game.spawns['Spawn1'].spawnCreep([WORK,CARRY,MOVE], newName, {memory: {role: 'upgrader'}});
        while (totalAccruedCost < currentRoomEnergy){

            if((numWork - numCarry) < 1){ // prioritize work over Carry to reduce working time
                numMove++;
                numWork++;
            }
            else { // this means (Carry + 2) = Work, so now add a Carry
                numMove++;
                numCarry++;
            }

            var totalAccruedCost = (costMove * numMove) + (costCarry * numCarry) + (costWork * numWork); //recompute
            if (totalAccruedCost > currentRoomEnergy){ // if we overdid our boundary cost, reset back to what we started with
                
                if ((numWork - numCarry) < 1){ //if this is true, we just added Work, so undo that
                    numMove--;
                    numWork--;
                }
                else { //if this is true, we just added Carry, so undo that
                    numMove--;
                    numCarry--;
                }

            }
        }

        //protection against having a minimum viable worker
        //in the event of population collapse
        //just hard reset it to 1:1:1 which costs 200 energy
        if ((numCarry < 1) || (numMove < 1) || (numWork < 1)){
            numWork = 1;
            numCarry = 1;
            numMove = 1;
        }

        // now that we've competed the numbers of each, build the array of parts to add to the creep
        // do Work, Carry, Move in that order (work gets attacked first, move gets attacked last)
        for(var nw = 0; nw < numWork; nw++){
            partsArray.push(WORK);
        }
        for(var nc = 0; nc < numCarry; nc++){
            partsArray.push(CARRY);
        }
        for(var nm = 0; nm < numMove; nm++){
            partsArray.push(MOVE);
        }

        console.log(partsArray);

        // spawn the creep
        Game.spawns['Spawn1'].spawnCreep(partsArray, String(workerRole + Game.time), {memory: {role: workerRole, targetid: '6213932b15cd3e8dce8d17d4'}}, {memory: {needsToFillEnergy: false}}, {memory: {extrasFull: false}});
    }
};

module.exports = workerSpawner;