var actionHarvest = require('action.harvest');
var utilExtraSources = require('util.extraSources');
var utilDepositEnergy = require('util.depositEnergy');



var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
        var extraSources = utilExtraSources.run(creep, false);
        var roomFull = ((creep.room.energyCapacityAvailable - creep.room.energyAvailable) == 0);


        // if empty or full, set appropriate memory value
        if(creep.store.getFreeCapacity(RESOURCE_ENERGY) == 0){
            creep.memory.needsToFillEnergy = false;
        }
        else if(creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0){
            creep.memory.needsToFillEnergy = true;
            actionHarvest.run(creep);
        }
        
        // determine if our extra sources (S's and C's) are also completely full
        for(var es in extraSources){
            if(extraSources[es].store.getFreeCapacity(RESOURCE_ENERGY) == 0){
                creep.memory.extrasFull = true;
                break;
            }
            else{
                creep.memory.extrasFull = false;
            }
        }


        
        // idle if room is full and extra sources are also full
        // only idle if full
        if(roomFull && creep.memory.extrasFull && (creep.store.getFreeCapacity(RESOURCE_ENERGY) == 0)){
            //First determine which of our total flags in game is our harvesterIdle flag in the same room as the creep
            for (var i = 0; i < flags.length; i++){
                if((creep.room == flags[i].room) && (flags[i].name == ('harvesterIdle_' + String(creep.room.name)))){
                    var roomHarvesterIdleFlag = flags[i];
                }
            }

            // if creep storage is full AND the room's storage is full, move to the idle position
            if(creepFull && roomFull){
                creep.moveTo(roomHarvesterIdleFlag);
            }
        }

        // now the harvesting/depositing behavior
        // deposit first
        if ((creep.store.getUsedCapacity(RESOURCE_ENERGY) > 0) && (creep.memory.needsToFillEnergy == false) && !creep.memory.extrasFull){
            utilDepositEnergy.run(creep, false);
        }
        else if(creep.memory.needsToFillEnergy == true){
            actionHarvest.run(creep);
        }
        

	}
};

module.exports = roleHarvester;