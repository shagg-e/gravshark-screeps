
var utilHeatMap = {

    /** 
     * @param draw
     *  
     * **/
    run: function(draw) {
	    
        for (var room in Game.rooms){

            if(!(Game.rooms[room].memory.heatmap)){ // if this room doesn't have a heatmap yet, generate a blank one
                var arrayRoom = []; 
                for (var i = 0; i < 50; i++){
                    var arrayY = [];
                    for (var j = 0; j < 50; j++){
                        arrayY.push(0);
                    }
                    arrayRoom.push(arrayY);
                }
                Game.rooms[room].memory.heatmap = arrayRoom;            
            }
            
            //store current heatmap to manipulate
            var roomHeatmap = Game.rooms[room].memory.heatmap;


            for(var i in Game.creeps){

                // create lastX and lastY if they don't exist yet
                // this is used to ignore idle creeps when building
                // the heatmap
                if(!Game.creeps[i].memory.lastX){
                    Game.creeps[i].memory.lastX = 0;
                    Game.creeps[i].memory.lastY = 0;
                }

                var roomX = Game.creeps[i].pos.x; // store xy of current creep
                var roomY = Game.creeps[i].pos.y;

                // only operate if the creep is in the room we're iterating through
                // and if the creep has moved (avoid racking up idle increments)
                if((room = Game.creeps[i].room) && ((roomX != Game.creeps[i].memory.lastX) || (roomY != Game.creeps[i].memory.lastY))){ 
                    
                    var currentCount = roomHeatmap[roomX][roomY]; // store current count
                    currentCount++;
                    Game.rooms[room.name].memory.heatmap[roomX][roomY] = currentCount; // reset current count
                    Game.creeps[i].memory.lastX = roomX; // store these for the next loop
                    Game.creeps[i].memory.lastY = roomY;
                    
                }
            }

            if(!draw){
                // this is an old printout of the raw array over the screen, rather than the draw function
                // for(var x = 0; x < 50; x++){ // print heatmap over screen. Just text for now as proof of concept
                //     for(var y = 0; y < 50; y++){
                //         curVal = roomHeatmap[x][y];
                //         Game.rooms[room.name].visual.text(curVal, x, y);

                //     }
                // }
                break;
            }

            var roomHeatmap = Game.rooms[room.name].memory.heatmap; // pull current (updated) heatmap
            var maxValue = 0;            
            // grab max value
            for(var x = 0; x < 50; x++){
                for (var y = 0; y < 50; y++){
                    if (roomHeatmap[x][y] > maxValue){
                        maxValue = roomHeatmap[x][y];
                    }
                }
            }
            // now set min to max and increment it down from there
            var minValue = maxValue;
            for(var x = 0; x < 50; x++){
                for (var y = 0; y < 50; y++){
                    if ((roomHeatmap[x][y] < minValue) && (roomHeatmap[x][y] > 0)){
                        minValue = roomHeatmap[x][y];
                    }
                }
            }


            // heatmap of colors ranging cold to hot
            var heatmapColors = [
                ['#125BBD', minValue],
                ['#129ebd'],
                ['#12bd96'],
                ['#12bd58'],
                ['#53d815'],
                ['#a6d815'],
                ['#d8c915'],
                ['#d89a15'],
                ['#d86515'],
                ['#d81515', maxValue]
            ]

            // do the math for the cutoff of each heatmap step on the palette
            var cutoffIncrement = Math.round(((maxValue - minValue) / heatmapColors.length));

            // iterate through and increment the cutoff
            // to know how to assign which color to a value
            // in the main draw logic
            var currentStep = cutoffIncrement;

            // add key values to the heatmapColors object
            // skip first and last since we always set those to min/max
            for (var i = 1; i < (heatmapColors.length - 1); i++){
                heatmapColors[i].push(currentStep);
                currentStep += cutoffIncrement;
            }



            // main draw logic
            var color = '';

            for(var x = 0; x < 50; x++){
                for (var y = 0; y < 50; y++){
                    if(roomHeatmap[x][y] >= minValue){

                        for (var z = 0; z < heatmapColors.length; z++){
                                var oneMore = z + 1;

                                if((maxValue - cutoffIncrement) < roomHeatmap[x][y]){
                                    Game.rooms[room.name].visual.rect((x - 0.5), (y - 0.5), 1, 1, {fill: '#d81515'});
                                    break;
                                }

                                if(heatmapColors[z][1] <= roomHeatmap[x][y] && roomHeatmap[x][y] <= heatmapColors[oneMore][1]){
                                    var color = heatmapColors[z][0];
                                    Game.rooms[room.name].visual.rect((x - 0.5), (y - 0.5), 1, 1, {fill: color});
                                    break;
                                }
                        }
                    }
                    else{
                        Game.rooms[room.name].visual.rect((x - 0.5), (y - 0.5), 1, 1, {fill: "#181596"});
                    }
                }    
            }
        }
    }

}
;

module.exports = utilHeatMap;