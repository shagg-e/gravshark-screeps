var actionHarvest = require('action.harvest');
var utilRepairTargets = require('util.repairTargets');


var roleMaintainer = {

    /** @param {Creep} creep **/
    run: function(creep) {
        var flags = Object.values(Game.flags);

        // old idle logic until we build out functionized idle behavior
        // //First determine which of our total flags in game is our harvesterIdle flag in the same room as the creep
        // for (var i = 0; i < flags.length; i++){
        //     if((creep.room == flags[i].room) && (flags[i].name == ('harvesterIdle_' + String(creep.room.name)))){
        //         var roomHarvesterIdleFlag = flags[i];
        //     }
        // }

        var roomStructs = utilRepairTargets.run(creep, false); // false means we don't want walls included
        var repairTarget = roomStructs[0]; //init
        

        for (var i in roomStructs){ // go through structures, assign weakest to repairTarget
            if((roomStructs[i].structureType == STRUCTURE_CONTAINER) || (roomStructs[i].structureType == STRUCTURE_STORAGE)){
                repairTarget = roomStructs[i];
            }
            else if (roomStructs[i].hits < repairTarget.hits){
                repairTarget = roomStructs[i];
            }

        }

        for(var i in roomStructs){
            if (roomStructs[i] == repairTarget){
                creep.room.visual.text("🧰🎯", repairTarget.pos, {align: 'left', font: '0.3'});
            } else{
                creep.room.visual.text("🧰", roomStructs[i].pos, {align: 'left', font: '0.3'});

            }
        }

        // // move to and do the repair
        // if(creep.repair(repairTarget) == ERR_NOT_IN_RANGE) {
        //     creep.moveTo(repairTarget, {visualizePathStyle: {stroke: '#ffaa00'}});
        // }

        


        // first deal with being between the controller and the energy source
        var sources = creep.room.find(FIND_SOURCES);

        if(creep.pos.getRangeTo(sources[0]) > 1 && creep.pos.getRangeTo(repairTarget) > 1){
            if(creep.store.getUsedCapacity(RESOURCE_ENERGY) > 0){ // if we have stuff
                creep.moveTo(repairTarget); // head to the target
            }
            else {
                creep.moveTo(sources[0]); // otherwise head to pick up energy
            }

        }

        // if in range of the target
        if(creep.pos.getRangeTo(repairTarget) == 1){
            if(creep.store.getUsedCapacity(RESOURCE_ENERGY) > 0){ // if we have stuff
                creep.repair(repairTarget); // repair target
            }
            else {
                creep.moveTo(sources[0]); // otherwise head to the source
            }
        }

        // if in range of the energy source
        if(creep.pos.getRangeTo(sources[0]) == 1){
            if(creep.store.getFreeCapacity(RESOURCE_ENERGY) > 0){ // if we have any free space
                actionHarvest.run(creep); // continue to harvest
            }
            else { //if we don't have free space
                creep.moveTo(repairTarget); // go back to the target
            }
        }

    }
                    

}
	    




module.exports = roleMaintainer;