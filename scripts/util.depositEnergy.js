

var utilDepositEnergy = {

    /**
     * @param {Creep} creep
     * @param canUse
     */

    run: function(creep, canUse){

        var utilExtraSources = require('util.extraSources');
        extraSources = utilExtraSources.run(creep, canUse);

        if ((extraSources.length > 0) && (creep.store.getUsedCapacity(RESOURCE_ENERGY) > 0)){ // Deposit into extraSources
            if(creep.transfer(extraSources[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                creep.moveTo(extraSources[0], {visualizePathStyle: {stroke: '#ffffff'}});
            }
        }
        else if(creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0){
            creep.memory.needsToFillEnergy = true;
        }

    }

};

module.exports = utilDepositEnergy;