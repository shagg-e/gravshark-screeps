var utilExtraSources = require('util.extraSources');



var rolePorter = {

    /** @param {Creep} creep **/
    run: function(creep) {
	    
        var extraSources = utilExtraSources.run(creep, true);

        // get array of spawn related storages in the room (Spawns + Extensions)
        // that have any empty space at all
        var spawnStorages = creep.room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType == STRUCTURE_EXTENSION || structure.structureType == STRUCTURE_SPAWN) &&
                    structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0;
            }
        });

        // if there are any spawn storages, prioritize porting resources to those
        // otherwise, fill up any towers
        var porterTarget;
        if(spawnStorages.length > 0){
            porterTarget = spawnStorages[0];
        }
        else{
            var towers = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_TOWER) &&
                        structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0;
                }
            });
            porterTarget = towers[0];
        }


        // first deal with being between the dropoff and the extra source

        if(creep.pos.getRangeTo(extraSources[0]) > 1 && creep.pos.getRangeTo(porterTarget) > 1){
            if((creep.store.getUsedCapacity(RESOURCE_ENERGY) > 0) && !(creep.memory.needsToFillEnergy == true)){ // if we're full
                creep.moveTo(porterTarget, {visualizePathStyle: {stroke: '#ffaa00'}}); // head to the dropoff point
            }
            else if(creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0){
                creep.moveTo(extraSources[0], {visualizePathStyle: {stroke: '#ffaa00'}}); // otherwise head to pick up energy
            }

        }

        // if in range of the dropoff point
        if(creep.pos.getRangeTo(porterTarget) == 1){
            if(creep.store.getUsedCapacity(RESOURCE_ENERGY) > 0){ // if we have stuff
                creep.transfer(porterTarget, RESOURCE_ENERGY); // deposit
            }
            else if(creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0){
                creep.moveTo(extraSources[0], {visualizePathStyle: {stroke: '#ffaa00'}}); // otherwise head to the source
            }
        }

        // if in range of the extra source
        if(creep.pos.getRangeTo(extraSources[0]) <= 1){
            if((creep.store.getFreeCapacity(RESOURCE_ENERGY) > 0) && (extraSources.length > 0)){ // if we have any free space
                creep.withdraw(extraSources[0], RESOURCE_ENERGY); // continue to harvest
            }
            else if(creep.store.getFreeCapacity(RESOURCE_ENERGY) == 0) { //if we don't have free space
                creep.moveTo(porterTarget, {visualizePathStyle: {stroke: '#ffaa00'}}); // go back to the target
            }
        }

	}
};

module.exports = rolePorter;