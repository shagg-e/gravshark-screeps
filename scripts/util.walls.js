
var utilWalls = {

    /** 
     * @param {Creep} creep
     * **/
    run: function(creep) {
        var utilWalls = creep.room.find(FIND_STRUCTURES, { 
            filter: (structure) => {
                    return (structure.hits < structure.hitsMax) && (structure.hits > 0) && (structure.hits < 100000) && ((structure.structureType == STRUCTURE_WALL) || (structure.structureType == STRUCTURE_RAMPART));
            }
         });
	    return utilWalls;

	}
};




module.exports = utilWalls;