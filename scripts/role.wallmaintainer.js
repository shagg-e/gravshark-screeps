var utilWalls = require('util.walls');
var utilExtraSources = require('util.extraSources');


var roleWallMaintainer = {

    /** @param {Creep} creep **/
    run: function(creep) {

        var roomStructs = utilWalls.run(creep); // true also includes walls
        // if(typeof repairTarget !== 'undefined'){
        //     console.log("RT undefined");
        //     var repairTarget = roomStructs[0]; //init
        // }

        // initialize memory objects if they don't exist
        // if one doesn't exist, the rest won't either
        if(!creep.memory.needsResources){
            creep.memory.needsResources = false;

        }
        if(creep.memory.needsNewTarget == undefined){
            creep.memory.needsNewTarget = false;
        }
        if(creep.memory.targetid == undefined){
            creep.memory.targetid = '6213932b15cd3e8dce8d17d4';

        }

        // force set needsResources
        if(creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0){
            creep.memory.needsResources = true;
        }
        else if(creep.store.getFreeCapacity(RESOURCE_ENERGY) == 0){
            creep.memory.needsResources = false;
        }



        // get repair target
        if(creep.memory.needsNewTarget == true){
            for (var i in roomStructs){

                if((roomStructs[i].hits < Game.getObjectById(creep.memory.targetid).hits)){ // only check if current is weaker
                    creep.memory.targetid = roomStructs[i].id;
                }
            }
            creep.memory.needsNewTarget = false;
        }

        // draw a target on the current repair target
        creep.room.visual.text("🧰🎯 Hits: " + Game.getObjectById(creep.memory.targetid).hits, Game.getObjectById(creep.memory.targetid).pos, {align: 'left', font: '0.3'});

        // first deal with being between the controller and the energy source
        var extraSources = utilExtraSources.run(creep, true);
        if(creep.pos.getRangeTo(extraSources[0]) > 1 && creep.pos.getRangeTo(Game.getObjectById(creep.memory.targetid)) > 1){
            if(creep.memory.needsResources == false && Game.getObjectById(creep.memory.targetid).hits < 100000){ // if we don't need energy
                creep.moveTo(Game.getObjectById(creep.memory.targetid), {visualizePathStyle: {stroke: '#ffaa00'}}); // head to the target

            }
            else if (creep.memory.needsResources == true){
                creep.moveTo(extraSources[0], {visualizePathStyle: {stroke: '#ffaa00'}}); // otherwise head to pick up energy
            }

        }

        // if in range of the target
        else if(creep.pos.getRangeTo(Game.getObjectById(creep.memory.targetid)) == 1){
            if(creep.store.getUsedCapacity(RESOURCE_ENERGY) > 0){ // if we have stuff
                creep.repair(Game.getObjectById(creep.memory.targetid)); // repair target
            }
            else {
                creep.moveTo(extraSources[0], {visualizePathStyle: {stroke: '#ffaa00'}}); // otherwise head to the source

            }
        }

        // if in range of the energy storage
        else if(creep.pos.getRangeTo(extraSources[0]) == 1){
            if(creep.store.getFreeCapacity(RESOURCE_ENERGY) > 0){ // if we have any free space
                creep.withdraw(extraSources[0], RESOURCE_ENERGY); // continue to harvest

            }
            else { // once we are full
                creep.moveTo(Game.getObjectById(creep.memory.targetid), {visualizePathStyle: {stroke: '#ffaa00'}}); // go back to the target
                creep.memory.needsNewTarget = true;
            }
        }
        
	}
};

module.exports = roleWallMaintainer;