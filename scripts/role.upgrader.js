var actionHarvest = require('action.harvest');



var roleUpgrader = {

    /** @param {Creep} creep **/
    run: function(creep) {
	    
        var sources = creep.room.find(FIND_SOURCES);
        

        // first deal with being between the controller and the energy source
        if(creep.pos.getRangeTo(sources[0]) > 1 && creep.pos.getRangeTo(creep.room.controller) > 1){
            if(creep.store.getFreeCapacity(RESOURCE_ENERGY) == 0){ // if we're full
                creep.moveTo(creep.room.controller); // head to the controller
            }
            else {
                creep.moveTo(sources[0]); // otherwise head to pick up energy
            }

        }

        // if in range of the controller
        if(creep.pos.getRangeTo(creep.room.controller) == 1){
            if(creep.store.getUsedCapacity(RESOURCE_ENERGY) > 0){ // if we have stuff
                creep.upgradeController(creep.room.controller); // upgrade controller
            }
            else {
                creep.moveTo(sources[0]); // otherwise head to the source
            }
        }

        // if in range of the energy source
        if(creep.pos.getRangeTo(sources[0]) == 1){
            if(creep.store.getFreeCapacity(RESOURCE_ENERGY) > 0){ // if we have any free space
                actionHarvest.run(creep); // continue to harvest
            }
            else { //if we don't have free space
                creep.moveTo(creep.room.controller); // go back to the controller
            }
        }

	}
};

module.exports = roleUpgrader;